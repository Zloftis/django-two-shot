from django.shortcuts import render, redirect
from .forms import FormLogin, SignUp
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User


def user_login(request):
    if request.method == "POST":
        form = FormLogin(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(
                    request,
                    username=username,
                    password=password
                )

            login(request, user)
            return redirect("home")
        else:
            form.add_error("The passwords do not match")
    else:
        form = FormLogin()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)

    return redirect("login")


def Sign_up(request):
    if request.method == "POST":
        form = SignUp(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password_confirmation == password_confirmation:
                user = User.objects.create_user(
                    username=username,
                    password=password
                )

                login(request, user)
                return redirect("home")
            else:
                form.add_error("The passwords do not match")
    else:
        form = SignUp()
    context = {"form": form}
    return render(request, "accounts/signup.html", context)
